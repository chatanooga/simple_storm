# StormTest Client API
import stormtest.ClientAPI as StormTest
import os
# Test environment definitions
# import TestEnvironment
# Serial port parameters
logParam = [57600, 8, "none", 1, "none", "TestName"]
# Test Code
def test():
    # Connect to the StormTest server 
    StormTest.ConnectToServer(os.environ['COMPUTERNAME'], "Hello World Test")
    # Reserves a slot on the server and start serial logging 
    StormTest.ReserveSlot( 1, 'default', logParam, False )
    # Print "Hello World" on console
    print "Hello World"
    # Wait 3 seconds
    StormTest.WaitSec(3)
    # End of test - release slots back to the server StormTest.ReleaseServerConnection()
    # Return test results
    return (StormTest.TM.PASS)

if __name__ == '__main__': 
    
    StormTest.ReturnTestResult(test())